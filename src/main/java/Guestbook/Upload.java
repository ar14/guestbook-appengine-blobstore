package Guestbook;

//file Upload.java

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;

public class Upload extends HttpServlet {
 private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

 @Override
 public void doPost(HttpServletRequest req, HttpServletResponse res)
     throws ServletException, IOException {
	 
     Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(req);
     
     List<BlobKey> blobKeys = blobs.get("myFile");

     if (blobKeys == null || blobKeys.isEmpty()) {
         res.sendRedirect("/");
     } else {
    	 //todo add the user
    	 ImagesService imagesService = ImagesServiceFactory.getImagesService();
    	 ServingUrlOptions servingOptions = ServingUrlOptions.Builder.withBlobKey(blobKeys.get(0));
    	 String servingUrl = imagesService.getServingUrl(servingOptions);
    	 res.sendRedirect(servingUrl);
    	// res.sendRedirect("/serve?blob-key=" + blobKeys.get(0).getKeyString());
     }
 }
}